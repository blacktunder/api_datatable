import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    strict:true,
    state:{
        datalist:[],
        categorylist:["Animals","Anime","Anti-Malware","Art & Design","Books","Business"],
    },
    mutations: {
        addDataList(state,list){
            state.datalist.push(list);
        },
        addData(state,data){
            state.datalist[0].push(data);
        },
        deleteRow(state,index){
            state.datalist[0].splice(index,1);
        },
        sortTable(state,list){
            //console.log("StoreTable: "+list.sortKey+" "+list.reverse);
            if(list.reverse && list.sortKey=='API'){
                function compare(a,b){
                    if(a.API>b.API){ return 1; }
                    if(a.API<b.API){ return -1; }
                    return 0;
                }
                state.datalist[0].sort(compare);
            }
            if(!list.reverse && list.sortKey=='API'){
                function compare(a,b){
                    if(a.API<b.API){ return 1; }
                    if(a.API>b.API){ return -1; }
                    return 0;
                }
                state.datalist[0].sort(compare);
            }
            if(list.reverse && list.sortKey=='Category'){
                function compare(a,b){
                    if(a.Category>b.Category){ return 1; }
                    if(a.Category<b.Category){ return -1; }
                    return 0;
                }
                state.datalist[0].sort(compare);
            }
            if(!list.reverse && list.sortKey=='Category'){
                function compare(a,b){
                    if(a.Category<b.Category){ return 1; }
                    if(a.Category>b.Category){ return -1; }
                    return 0;
                }
                state.datalist[0].sort(compare);
            }
        },
        updateEdit(state,list){
            state.datalist[0][list.index].API = list.API;
            state.datalist[0][list.index].Auth = list.Auth;
            state.datalist[0][list.index].Category = list.Category;
            state.datalist[0][list.index].Description = list.Description;
            state.datalist[0][list.index].HTTPS = list.HTTPS;
            state.datalist[0][list.index].Link = list.Link;
        }
    },
})